module View.Attributes exposing (..)

import Dagre.Attributes as DA
import Dict exposing (..)
import Graph as G
import Render.StandardDrawers.Types as RSDT
import Color

type alias Attribute c =
    c -> c


type Shape
    = Box
    | Circle
    | Rbox Float
    | Ellipse


-- ==================== Dagre Attributes ==================== --


elemSep : Float -> Attribute { c | rankSep : Float }
elemSep f =
    \cc ->
        { cc | rankSep = f }


direction : DA.RankDir -> Attribute { c | direction : DA.RankDir }
direction f =
    \cc ->
        { cc | direction = f }


valsperRow : Int -> Attribute { c | valsperRow : Int }
valsperRow f =
    \cc ->
        { cc | valsperRow = f }


-- ==================== Render Attributes ==================== --


widthDict : List ( G.NodeId, Float ) -> Attribute { c | widthDict : Dict G.NodeId Float }
widthDict f =
    \cc ->
        { cc | widthDict = Dict.fromList f }


heightDict : List ( G.NodeId, Float ) -> Attribute { c | heightDict : Dict G.NodeId Float }
heightDict f =
    \cc ->
        { cc | heightDict = Dict.fromList f }


width : Float -> Attribute { c | width : Float }
width f =
    \cc ->
        { cc | width = f }


height : Float -> Attribute { c | height : Float }
height f =
    \cc ->
        { cc | height = f }


shape : Shape -> Attribute { c | shape : RSDT.Shape }
shape f =
    \cc ->
        { cc
            | shape =
                case f of
                    Box ->
                        RSDT.Box

                    Circle ->
                        RSDT.Circle

                    Rbox x ->
                        RSDT.RoundedBox x

                    Ellipse ->
                        RSDT.Ellipse
        }

labelDisp : (Float, Float) -> Attribute { c | labelDisp : (Float, Float) }
labelDisp f = 
    \cc ->
        { cc | labelDisp = f }



style : String -> Attribute { c | style : String }
style f = 
    \cc -> 
        { cc | style = f }

background : Color.Color -> Attribute { c | background : Color.Color }
background f =
    \cc ->
        { cc | background = f }

marginX : Float -> Attribute { c | marginX : Float }
marginX f = 
    \cc ->
        { cc | marginX = f }

marginY : Float -> Attribute { c | marginY : Float }
marginY f = 
    \cc ->
        { cc | marginY = f }
