module Dagre.ConfigTypes exposing (..)

{-| This module exposes the Drawer configuration Types. -}

import Color exposing (Color)
import Graph exposing (Node)
import Render.StandardDrawers.Types exposing (..)


{- This type represents all the attributes configurable for the
   standard Node Drawer
-}


type alias NodeDrawerConfig n msg =
    { label : Node n -> String
    , shape : Shape
    , onClick : Maybe (Node n -> msg)
    , strokeColor : Node n -> Color
    , strokeWidth : Node n -> Float
    , strokeDashArray : Node n -> String
    , style : Node n -> String
    , fill : Node n -> Color
    , title : Node n -> String
    , xLabel : Node n -> String
    , xLabelPos : Node n -> Float -> Float -> ( Float, Float )
    }
