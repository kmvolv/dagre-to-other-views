module Main exposing (..)

import Html
import Array exposing (Array, length)
import Dagre.Attributes as DA
import Dict exposing (..)
import Graph as G
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import List exposing (indexedMap, range)
import Render as R
import Render.StandardDrawers as RSD
import Render.StandardDrawers.Attributes as RSDA
import ArrayRender.Drawer as AD
import ArrayRender.Configs as ACF
import Svg exposing (..)
import View.Attributes as VA
import String
import Color


-- Input array


array : Array Int
array =
    Array.fromList <| [ 1, 3, 5, 7, 9, 11, 13, 15 ]

main : Html msg
main = 
    Html.div
        [ HA.style "text-align" "center" ]
        [
            stylesheet
            , viewArr
                [ 
                  VA.elemSep 50
                , VA.direction DA.LR
                , VA.valsperRow 0
                ]
                [
                    VA.style "fill: black;"
                    , VA.width 50
                    , VA.height 40
                    , VA.marginX 50
                    , VA.marginY 50
                    , VA.shape VA.Box
                    , VA.background Color.lightBlue
                    , VA.labelDisp (0,40)
                ]
                array
        ]



-- ================= INTERNAL HELPERS ==================== --


genEdges :
    Int
    -> Int
    -> List ( G.NodeId, G.NodeId ) -- Function to generate edges of linear graph
genEdges brpoint len =
    let
        filterfunc ( x, y ) =
            if modBy brpoint y == 0 then
                False

            else
                True
    in
    indexedMap Tuple.pair (range 1 len)
        |> List.filter filterfunc


conv2graph :
    Int
    -> Array a
    -> G.Graph a () -- Function to convert array to a linear graph
conv2graph brpoint arr =
    let
        breakpoint =
            case brpoint of
                0 ->
                    Array.length arr

                x ->
                    x
    in
    G.fromNodeLabelsAndEdgePairs
        (Array.toList <| arr)
        (genEdges breakpoint (length arr))


viewArr : List (VA.Attribute ACF.DagreConfig) -> List (VA.Attribute ACF.RenderConfig) -> Array Int -> Html.Html msg
viewArr modsD modsR arr =
    let
        configD =
            List.foldl (\f a -> f a) ACF.defDagreConfig modsD

        configR = 
            List.foldl (\f a -> f a) ACF.defRenderConfig modsR

        (xdisp,ydisp) = configR.labelDisp
    in
    R.draw
        [ DA.widthDict configR.widthDict
        , DA.heightDict configR.heightDict
        , DA.width configR.width
        , DA.height (configR.height)
        , DA.rankSep configD.rankSep
        , DA.rankDir configD.direction 
        , DA.marginX configR.marginX
        , DA.marginY configR.marginY
        ]
        [ 
        R.nodeDrawer
            (AD.drawElement 
                [ RSDA.label (\n -> String.fromInt n.label)
                , RSDA.shape configR.shape
                , RSDA.xLabel (\n -> String.fromInt n.id)
                , RSDA.xLabelPos (\_ _ _ -> (xdisp,ydisp))
                , RSDA.fill (\_ -> configR.background)
                ] 
                (configR)
            ) 
        , R.edgeDrawer
            (RSD.svgDrawEdge
                [ RSDA.strokeWidth (\_ -> 0)
                ]
            )
        , R.style configR.style
        ]   
        (conv2graph configD.valsperRow arr)


stylesheet : Html msg
stylesheet =
    let
        tag =
            "link"

        attrs =
            [ attribute "Rel" "stylesheet"
            , attribute "property" "stylesheet"
            , attribute "href" "styles.css"
            ]

        children =
            []
    in
    Html.node tag attrs children