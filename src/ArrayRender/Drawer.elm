module ArrayRender.Drawer exposing (drawElement)

import Color
import Graph exposing (Node)
import Render.StandardDrawers.Attributes exposing (Attribute)
import Dagre.ConfigTypes exposing (..)
import ArrayRender.Configs as C
import Render.StandardDrawers.Types exposing (..)
import Render.Types exposing (..)
import TypedSvg as TS 
import Svg as S
import TypedSvg.Attributes as TA
import TypedSvg.Core as TC exposing (Svg)
import TypedSvg.Events as TE
import TypedSvg.Types as TT
import List 

defElemDrawerConfig : NodeDrawerConfig n msg
defElemDrawerConfig =
    let
        f =
            \n -> String.fromInt n.id

        f_ =
            \_ -> ""
    in
    { label = f
    , shape = Ellipse
    , onClick = Nothing
    , strokeColor = \_ -> Color.blue
    , strokeWidth = \_ -> 1
    , strokeDashArray = f_
    , style = f_
    , fill = \_ -> Color.rgb255 178 235 242
    , title = f
    , xLabel = f_
    , xLabelPos = \_ w h -> ( (w / 2) + 1, (-h / 2) - 1 )
    }


drawElement : List (Attribute (NodeDrawerConfig n msg)) -> C.RenderConfig -> NodeDrawer n msg
drawElement edits rConfig nodeAtrib =
    let
        node =
            nodeAtrib.node

        ( posX, posY ) =
            nodeAtrib.coord

        config =
            List.foldl (\f a -> f a) defElemDrawerConfig edits

        lbl =
            config.label node

        nodeId =
            "node-" ++ String.fromInt node.id

        gAtrib =
            case config.onClick of
                Nothing ->
                    [ TA.id nodeId
                    , TA.class [ "node" ]
                    , TA.style <| config.style node
                    ]

                Just f ->
                    [ TA.id nodeId
                    , TA.class [ "node" ]
                    , TA.style <| config.style node
                    , TE.onClick (f node)
                    , TA.cursor TT.CursorPointer
                    ]
    in
    S.g
        gAtrib
        [ TS.title [] [ TC.text <| config.title node ]
        , containerShapeDraw config nodeAtrib
        , TS.text_
            [ TA.textAnchor TT.AnchorMiddle
            , TA.dominantBaseline TT.DominantBaselineCentral
            , TA.transform [ TT.Translate posX posY ]
            ]
            [ TC.text lbl ]
        , xLabelDrawer config.xLabel config.xLabelPos nodeAtrib
        ]



containerShapeDraw : NodeDrawerConfig n msg -> NodeAttributes n -> Svg msg
containerShapeDraw config nodeAtrib =
    let
        ( posX, posY ) =
            nodeAtrib.coord

        width =
            nodeAtrib.width

        height =
            nodeAtrib.height

        d =
            max width height
    in
    case config.shape of
        Circle ->
            TS.circle
                [ TA.r <| TT.Px (d / 2)
                , TA.stroke <| TT.Paint <| config.strokeColor nodeAtrib.node
                , TA.strokeWidth <| TT.Px <| config.strokeWidth nodeAtrib.node
                , TA.strokeDasharray <| config.strokeDashArray nodeAtrib.node
                , TA.fill <| TT.Paint <| config.fill nodeAtrib.node
                , TA.cx <| TT.Px posX
                , TA.cy <| TT.Px posY
                ]
                []

        Ellipse ->
            TS.ellipse
                [ TA.rx <| TT.Px (width / 2)
                , TA.ry <| TT.Px (height / 2)
                , TA.stroke <| TT.Paint <| config.strokeColor nodeAtrib.node
                , TA.strokeWidth <| TT.Px <| config.strokeWidth nodeAtrib.node
                , TA.strokeDasharray <| config.strokeDashArray nodeAtrib.node
                , TA.fill <| TT.Paint <| config.fill nodeAtrib.node
                , TA.cx <| TT.Px posX
                , TA.cy <| TT.Px posY
                ]
                []

        Box ->
            TS.rect
                [ TA.width <| TT.Px width
                , TA.height <| TT.Px height
                , TA.stroke <| TT.Paint <| config.strokeColor nodeAtrib.node
                , TA.strokeWidth <| TT.Px <| config.strokeWidth nodeAtrib.node
                , TA.strokeDasharray <| config.strokeDashArray nodeAtrib.node
                , TA.fill <| TT.Paint <| config.fill nodeAtrib.node
                , TA.x <| TT.Px (posX - width / 2)
                , TA.y <| TT.Px (posY - height / 2)
                ]
                []

        RoundedBox r ->
            TS.rect
                [ TA.width <| TT.Px width
                , TA.height <| TT.Px height
                , TA.rx <| TT.Px r
                , TA.stroke <| TT.Paint <| config.strokeColor nodeAtrib.node
                , TA.strokeWidth <| TT.Px <| config.strokeWidth nodeAtrib.node
                , TA.strokeDasharray <| config.strokeDashArray nodeAtrib.node
                , TA.fill <| TT.Paint <| config.fill nodeAtrib.node
                , TA.x <| TT.Px (posX - width / 2)
                , TA.y <| TT.Px (posY - height / 2)
                ]
                []


xLabelDrawer : (Node n -> String) -> (Node n -> Float -> Float -> ( Float, Float )) -> NodeDrawer n msg
xLabelDrawer xlbl xLabelPos nodeAtrib =
    let
        ( posX, posY ) =
            nodeAtrib.coord

        ( xPosX, xPosY ) =
            xLabelPos nodeAtrib.node nodeAtrib.width nodeAtrib.height

        ( xlPosX, xlPosY ) =
            ( posX + xPosX, posY + xPosY )
    in
    TS.text_
        [ TA.textAnchor TT.AnchorMiddle
        , TA.dominantBaseline TT.DominantBaselineCentral
        , TA.transform [ TT.Translate xlPosX xlPosY ]
        , TA.fontFamily ["monospace"]
        , TA.fontSize <| TT.em 0.8
        ]
        [ TC.text (xlbl nodeAtrib.node)
        ]

