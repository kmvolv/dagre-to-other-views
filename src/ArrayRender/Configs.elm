module ArrayRender.Configs exposing (..)

import Dict exposing (..)
import Graph as G
import Render.StandardDrawers.Types as RSDT
import Color
import Dagre.Attributes as DA

type alias RenderConfig = 
    {
        widthDict : Dict G.NodeId Float
        , heightDict : Dict G.NodeId Float 
        , width : Float
        , height : Float  
        , marginX : Float
        , marginY : Float
        , shape : RSDT.Shape
        , style : String
        , background : Color.Color
        , labelDisp : (Float, Float)
    }

defRenderConfig : RenderConfig 
defRenderConfig = 
    {
        widthDict = Dict.empty
        , heightDict = Dict.empty   
        , height = 60
        , width = 60
        , marginX = 0
        , marginY = 0
        , shape = RSDT.Box
        , style = ""
        , background = Color.rgb255 178 235 242
        , labelDisp = (0,25)
    }

type alias DagreConfig =
    { 
      rankSep : Float
    , direction : DA.RankDir
    , valsperRow : Int
    }

defDagreConfig : DagreConfig
defDagreConfig =
    { 
      rankSep = 50
    , direction = DA.TB
    , valsperRow = 0
    }